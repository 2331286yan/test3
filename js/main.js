var app = new Vue({
    el:'#app',
    data: {
        products:[
            {id: 1,title:'Hamlin ',short_text:'short1',Image:'22250_54472.jpg',desc:''},
            {id: 2,title:'Verna',short_text:'short2',Image:'22250_54473.jpg',desc:''},
            {id: 3,title:'Salustiana',short_text:'short3',Image:'22250_54474.jpg',desc:''},
            {id: 4,title:'Navelina',short_text:'short4',Image:'22250_54477.jpg',desc:''},
            {id: 5,title:'Moro',short_text:'short5',Image:'22250_54480.jpg',desc:''}
            ],
        cart:[],
        product:[],
        btnVisible:0,
    },
    methods:{
        getProduct(){
            if(window.location.hash){
                var id = window.location.hash.replace('#', '');
                if(this.products && this.products.length>0){
                    for(i in this.products){
                        if(this.products[i] && this.products[i].id && id==this.products[i].id)
                            this.product = this.products[i];
                    }
                }
            }
        },
        addToCart(id){
            var cart = [];
            if(window.localStorage.getItem('cart')){
                cart = window.localStorage.getItem('cart').split(',');
            }

            if(cart.indexOf(String(id))==-1){
                cart.push(id);
                window.localStorage.setItem('cart', cart.join());
                this.btnVisible = 1;
            }
        },
        checkInCart(){
            if(this.product && this.product.id && window.localStorage.getItem('cart').split(',').indexOf(String(this.product.id))!=-1)
            this.btnVisible=1;
        },
        getCart(){
          let lcart = window.localStorage.getItem('cart').split(',');
          for(let i=0 ;i<this.products.length;i++){
            if(lcart.indexOf(String(this.products[i].id)) != -1){
                this.cart.push(this.products[i]);
            }
          }
        }
    },
    mounted(){
    this.getProduct();
    this.checkInCart();
    this.getCart();
    }
    
});